const express = require('express');
const router = express.Router();
const data = require('./mongoDb');


router.get('/', async (req, res) => {
  if (req.query.url){
    res.status(301).redirect(req.query.url);
  }
  if (req.query.shorten){
  const urlLink = await data
    .getDb()
    .collection('shorturl')
    .findOne({shorten: `${req.query.shorten}`});

  if (urlLink === null){
    return null
  }
  return res.send(urlLink);
}
});



router.post('/', async (req, res, next) => {
  try {
    if (!req.body.url) {
      return res.status(404).send({message: 'text are required!'});
    }
    const urlLink = {
      url: req.body.url,
      shorten: req.body.shorten
    };

    const result = await data
      .getDb().collection('shorturl')
      .insertOne(urlLink);
    const _id = result.insertedId;

    return res.send({message: 'created new shorten link', _id});
  } catch (e) {
    next(e);
  }
});


module.exports = router;