const express = require('express');
const app = express();
const data = require('./mongoDb');
const shorten = require('./shorten');
const cors = require('cors');
const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));

app.use(express.json());
app.use('/shorten', shorten);

const run = async () => {
  await data.connect();
}

app.listen(port, () => {
  console.log('We are live on ' + port);
});

process.on('exit', () => {
  data.disconnect();
})

run().catch(e => console.error(e));