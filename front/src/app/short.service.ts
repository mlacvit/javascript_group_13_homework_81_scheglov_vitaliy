import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { map, Subject } from 'rxjs';
import { ShortModel } from './short.model';

@Injectable({
  providedIn: 'root'
})

export class ShortService {
  createFetch = new Subject<boolean>();
  short: ShortModel | null = null;
  shortChange = new Subject<ShortModel | null>();
  constructor(private http: HttpClient) { }


  createShort(value: ShortModel) {
    this.createFetch.next(true);
    return this.http.post(environment.apiUrl + '/shorten', value)
      .subscribe(() => {
        this.createFetch.next(false);
    });
  };

  getShort(shorten: string) {
    return this.http.get<ShortModel>(environment.apiUrl + `/shorten?shorten=${shorten}`)
      .pipe(map(result => {
        if (result === null) {
          return null;
        }
        return new ShortModel(
          result._id,
          result.url,
          result.shorten,
        );
      })).subscribe(result => {
        this.short = result;
        this.shortChange.next(this.short)
      });
  };

  redirectUrl(url: string){
   return this.http.get(environment.apiUrl + `/shorten?url=${url}`).subscribe();
  }

}
