export class ShortModel {
  constructor(
    public _id: string,
    public url: string,
    public shorten: string,
  ) {}
}

