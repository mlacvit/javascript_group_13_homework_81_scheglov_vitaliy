import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { customAlphabet } from 'nanoid'
import { ShortService } from '../short.service';
import {  ShortModel } from '../short.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  nanoid = customAlphabet('0123456789abcdefghijklmnopqrstuvwxyz', 5);
  fetchingSubscriptions!: Subscription;
  isFetching: boolean = false;
  shorten =  '';
  short: ShortModel | null = null;
  shortChange!: Subscription;
  constructor(private service: ShortService) { }

  ngOnInit(): void {
    this.fetchingSubscriptions = this.service.createFetch
      .subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });

  this.shortChange = this.service.shortChange.subscribe(result => {
    this.short = result;
  })

  }

  onSub() {
    this.form.value.shorten = this.nanoid();
    const value: ShortModel = this.form.value;
    this.service.createShort(value);
    this.service.getShort(value.shorten);
    this.form.reset();
  }

  getUrl(url:string){
   return this.service.redirectUrl(url);
  }

  ngOnDestroy(): void {
    this.fetchingSubscriptions.unsubscribe();
    this.shortChange.unsubscribe();
  }
}
